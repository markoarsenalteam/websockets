var gulp = require('gulp'),
	browserSync = require('browser-sync').create(),
	runSequence = require('run-sequence'),
	$ = require('gulp-load-plugins')();


gulp.task('sass', function () {
	var onError = function(err) {
		$.notify.onError({
			title:    "SASS ERROR",
			message:  'Message: ' + err.message,
			sound:    "Beep"
		})(err);
		this.emit('end');
	}

	gulp.src('./css/style.scss')
		.pipe($.plumber({ errorHandler: onError }))
		.pipe($.sourcemaps.init({ loadMaps: true }))
		.pipe($.sass({ outputStyle: 'compressed' }))
		.pipe($.autoprefixer({ browers: ['last 5 versions'] }))
		.pipe($.sourcemaps.write())
		.pipe(gulp.dest('./css'))
		.pipe(browserSync.reload({ stream: true }));
});

gulp.task('browser-sync', function() {
    browserSync.init({
        proxy: "http://localhost:3700"
    });
});

gulp.task('emoji', function () {
	gulp.src([
			'./script/vendors/emoji/tether.min.js',
			'./script/vendors/emoji/config.js',
			'./script/vendors/emoji/util.js',
			'./script/vendors/emoji/jquery.emojiarea.js',
			'./script/vendors/emoji/emoji-picker.js'
		])
		.pipe($.concat('emoji.min.js'))
		.pipe($.uglify())
		.pipe(gulp.dest('./script/vendors/emoji/'));
});

gulp.task('watch', function () {
	gulp.watch('./css/**/*.scss', ['sass']);
	gulp.watch("./*.html").on('change', browserSync.reload);
	gulp.watch("./script/**/*.js").on('change', browserSync.reload);
});

gulp.task('default', ['browser-sync', 'sass', 'watch']);