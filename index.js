var express = require('express'),
	uuid = require('node-uuid'),
	_ = require('underscore-node'),
	fs = require('file-system'),
	app = express(),
	port = 3700;
 
// Routing
app.use("/", express.static(__dirname));
 
// Socket io
var io = require('socket.io').listen(app.listen(port)),
	history = [];

console.log("Listening on port " + port);

var users = [],
	teams = [],
	questions = {},
	answers = {},
	gameIsInProgress = false;

io.sockets.on('connection', function (socket) {
	// Save user data
	socket.on('saveUserData', function (data) {
		var userData = data;

		if (data.guid) {
			users[data.guid] = data;
		} else {
			data.guid = uuid.v4();
			users[uuid.v4()] = data;
		}

		socket.emit('userDataSaved', userData);
	});

	// Chat
	socket.emit('history', history);

	socket.on('send', function (data) {
		// Message time
		var d = new Date(),
			datetext = d.toTimeString();

		datetext = datetext.split(' ')[0];
		data.time = datetext;

		// Merge messages
		var lastMessageData = history[history.length - 1];

		if (lastMessageData && lastMessageData.user.guid == data.user.guid) {
			lastMessageData.message += '<br />' + data.message;
			lastMessageData.lastMessage = data.message;
			lastMessageData.mergedMessages = true;
			
			io.sockets.emit('message', lastMessageData);
		} else {
			history.push(data);
			io.sockets.emit('message', data);
		}
	});

	socket.on('typing', function(user) {
		io.sockets.emit('typing', user);
	});

	// Game
	if (gameIsInProgress === true) {
		return false;
	}
	
	socket.emit('teams', teams);

	socket.on('joinTeam', function (data) {
		if (gameIsInProgress == true) {
			return false;
		}

		var team = data.team;

		switch (team) {
			case 1:
				teams[1] = teams[1] || [];
				teams[1].push(data);
				break;

			case 2:
				teams[2] = teams[2] || [];
				teams[2].push(data);
				break;

			case 3:
				teams[3] = teams[3] || [];
				teams[3].push(data);
				break;

			case 4:
				teams[4] = teams[4] || [];
				teams[4].push(data);
				break;
		}

		io.sockets.emit('teams', teams);
	});

	socket.on('leaveTeam', function (data) {
		if (gameIsInProgress == true) {
			return false;
		}

		var team = data.team;

		// Remove from team
		teams[team] = _.reject(teams[team], function (player) {
			return player.guid == data.guid;
		});

		io.sockets.emit('teams', teams);
	});

	socket.on('startTheGame', function() {
		if (gameIsInProgress == true) {
			return false;
		}

		gameIsInProgress = true;

		// Set total points to 0
		for (var team in teams) {
			teams[team].totalPoints = 0;
		}

		// Get the questions and the answers
		questions = JSON.parse(fs.readFileSync('files/questions.json', 'utf8'));
		answers = JSON.parse(fs.readFileSync('files/answers.json', 'utf8'));

		io.sockets.emit('gameStarted');

		startTheQuestions();
	});

	function startTheQuestions () {
		var i = 1,
			questionInterval = null,
			timeInterval = null;

		// Send question
		sendQuestion();

		function sendQuestion () {
			io.sockets.emit('question', questions[i]);

			var second = 10;

			timeInterval = setInterval(function () {
				io.sockets.emit('time', second);

				if (second === 0) {
					clearInterval(timeInterval);

					showAnswer(i);
					i++;

					if (i <= 30) {
						setTimeout(sendQuestion, 5000);
					} else {
						// Final results
						var lastAnswer = answers[30];

						for (var team in teams) {
							for (var a in lastAnswer.teamAnswers) {
								if (lastAnswer.teamAnswers[a].team == team) {
									teams[team].push({
										team: team,
										totalPoints: lastAnswer.teamAnswers[a].totalPoints
									})
								}
							}
						}

						setTimeout(function () {
							io.sockets.emit('gameOver');
						}, 3000);

						setTimeout(function () {
							io.sockets.emit('results', teams);
						}, 8000);
					}
				}

				second--;
			}, 1000);
		}
	}

	socket.on('answer', function(data) {
		var u = data.user,
			t = u.team,
			q = data.questionNumber;

		var isCorrect = (answers[q]['answer'] == data.answerNumber) ? true : false

		answers[q]['teamAnswers'][t] = {
			user: u,
			team: t,
			answer: data.answerNumber,
			isCorrect: isCorrect,
			finnishNumber: answers[q]['finnishNumber']++
		}
	});

	function showAnswer (question) {
		var a = answers[question],
			ta = answers[question].teamAnswers;

		// Check all answers
		for (var i = 4; i > 0; i--) {
			if (!ta[i]) {
				ta[i] = {
					team: i,
					isCorrect: false,
					finnishNumber: -1
				}
			}
		}

		// Calculate points
		var sa = _.sortBy(ta, 'finnishNumber'),
			positivePoints = 8,
			negativePoints = -1;

		sa.pop(); // Remove null element

		for (var an in sa) {
			if (sa[an].isCorrect === false && sa[an].finnishNumber === -1) {
				sa[an].points = -4;
			} else if (sa[an].isCorrect === true) {
				sa[an].points = positivePoints--;
			} else if (sa[an].isCorrect === false) {
				sa[an].points = negativePoints--;
			}

			// Total points
			if (teams[sa[an].team]) {
				var totalPoints = teams[sa[an].team].totalPoints + sa[an].points;

				teams[sa[an].team].totalPoints = totalPoints;
				sa[an].totalPoints = totalPoints;
			} else {
				sa[an].totalPoints = 0;
			}
		}

		a.teamAnswers = sa;
		io.sockets.emit('showAnswer', a);
	}


	// Presentation
	socket.on('prev', function() {
		io.sockets.emit('prev');
	});

	socket.on('next', function() {
		io.sockets.emit('next');
	});
});