/**
* Global namespace
*/
var IO = IO || {};


(function () {
	var $content = $('#content'),
		$chatTypingIndicator = $('#chatTypingIndicator'),
		$sendBtn = $('#sendBtn');

	// Emoji
	window.emojiPicker = new EmojiPicker({
		emojiable_selector: '[data-emojiable=true]',
		assetsPath: 'images/emoji/',
		popupButtonClasses: 'icon-happy',
		norealTime: false
	});

	window.emojiPicker.discover();

	var $inputField = $('#inputField'),
		$emojiEditorInput = $('.emoji-wysiwyg-editor');

	// User data
	IO.userData = localStorage.getItem('userData') ? JSON.parse(localStorage.getItem('userData')) : {};

	IO.socket.on('message', function (data) {
		if (data.message && data.user) {
			var html = createHtml([data]);

			// Append message
			if (data.mergedMessages == true) {
				$content.find('.js-user-message:last').replaceWith(html);
			} else {
				$content.append(html);
			}
			
			$content[0].scrollTop = $content[0].scrollHeight;

			// Sound
			IO.sounds.pop.pause();
			IO.sounds.pop.currentTime = 0;
			IO.sounds.pop.play();

			// Notification
			showNotification(data);
		} else {
			console.log("There is a problem:", data);
		}
	});

	// Load server history first time
	IO.socket.on('history', function (messages) {
		var html = createHtml(messages);

		$content.html(html);
		$content[0].scrollTop = $content[0].scrollHeight;
	});


	// Send message
	$sendBtn.on('click', sendMessage);

	$emojiEditorInput.on('keydown', function(e) {
		if (e.keyCode == 13) {
			sendMessage();
			return false;
		}
	});

	// Sounds
	$('#enableSoundBtn').on('click', function () {
		$('#enableSoundWrapper').hide();

		IO.sounds.pop.play();
		IO.sounds.pop.pause();
		IO.sounds.pop.currentTime = 0;
	});

	IO.sounds = {
		pop: new Audio('/sounds/pop.mp3'),
	}

	/**
	 * @param  { Array } 	History messages
	 * @return { String } 	Html for all messages
	 */
	function createHtml (messages) {
		var template = '<div class="media js-user-message {5}">\
							<div class="media-left">\
								<div class="media-object avatar" style="background-image: url(images/avatars/{0}.gif)"></div>\
							</div>\
							<div class="media-body">\
								<h4 class="media-heading">{1}</h4>\
								<p class="media-subheading text-muted">{2} <span class="message-time">{3}</span></p>\
								<p class="alert">{4}</p>\
							</div>\
						</div>';

		var html = '';

		messages.forEach(function (el, index) {
			if (el.user.guid == IO.userData.guid) {}
			var u = el.user,
				t = el.time,
				m = el.message,
				c = (el.user.guid == IO.userData.guid) ? 'curent-user' : '';

			// Parse emoji
			m = emojiPicker.unicodeToImage(m);

			html += template.format(u.avatarId, u.username, u.name, t, m, c);
		});

		return html;
	}

	function sendMessage () {
		var text = $inputField.val().trim();

		// Remove html tags
		text = removeHtmlTags(text);

		// Add links
		text = text.linkify();

		if (text.length > 0) {
			IO.socket.emit('send', {
				user: IO.userData,
				message: text
			});

			$inputField.val('');
			$emojiEditorInput.html('');
		}
	}

	// Remove html tags
	function removeHtmlTags (text) {
		return text.replace(/<(?:.|\n)*?>/gm, '');
	}

	// Notification
	function showNotification (data) {
		if (data.user.guid == IO.userData.guid) {
			return false;
		}

		if (window.Notification && Notification.permission !== "denied") {
			Notification.requestPermission(function(status) {  // status is "granted", if accepted by user
				var n = new Notification(data.user.username, { 
					body: removeHtmlTags(data.lastMessage || data.message),
					icon: '/images/avatars/' + data.user.avatarId + '.gif'
				});

				n.onclick = function () {
					window.focus();
					this.close();
				}

				setTimeout(function () {
					n.close();
				}, 3000);
			});
		}
	}

	// Typing indicator
	$emojiEditorInput.on('DOMSubtreeModified', function (e) {
		IO.socket.emit('typing', IO.userData);
	});

	var timeouts = {};

	IO.socket.on('typing', function(user) {
		if (user.guid != IO.userData.guid) {
			if (timeouts[user.guid]) {
				clearTimeout(timeouts[user.guid]);
				timeouts[user.guid] = null;
			} else {
				var html = '<span class="user-typing" id="' + user.guid + '">' + user.username + ' is typing...</span>';
				$chatTypingIndicator.append(html);
			}

			timeouts[user.guid] = setTimeout(function () {
				$('#' + user.guid).remove();
				timeouts[user.guid] = null;
			}, 1000);
		}
	});
}());