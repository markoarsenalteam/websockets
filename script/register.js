/**
* Global namespace
*/
var IO = IO || {};


(function () {
	var DOM = {
		$registerModal: $('#registerModal'),
		$usernamePanel: $('#usernamePanel'),
		$usernameInput: $('#usernameInput'),
		$usernameOkBtn: $('#usernameOkBtn'),
		$namePanel: $('#namePanel'),
		$nameInput: $('#nameInput'),
		$nameOkBtn: $('#nameOkBtn'),
		$avatarPanel: $('#avatarPanel'),
		$avatarsList: $('#avatarsList'),
		$successPanel: $('#successPanel')
	}

	// Open modal
	$('#registerBtn').on('click', function(e) {
		e.preventDefault();
		$('#registerModal').modal();
	});

	// User data
	IO.userData = localStorage.getItem('userData') ? JSON.parse(localStorage.getItem('userData')) : {};

	// Username
	DOM.$usernamePanel.validate();

	DOM.$usernameInput[0].focus();

	DOM.$usernameInput.on('keydown', function(e) {
		if (e.keyCode == 13) {
			e.preventDefault();
			processUsername();
		}
	});

	DOM.$usernameOkBtn.on('click', function(e) {
		processUsername();
	});

	function processUsername () {
		if (DOM.$usernamePanel.valid()) {
			IO.userData.username = DOM.$usernameInput.val();
			IO.saveUserData();

			DOM.$usernameInput.val('');
			DOM.$usernamePanel.hide();
			DOM.$namePanel.show();
			DOM.$nameInput[0].focus();
		}
	}

	// Name
	DOM.$nameInput.on('keydown', function(e) {
		if (e.keyCode == 13) {
			e.preventDefault();
			processName();
		}
	});

	DOM.$nameOkBtn.on('click', function(e) {
		processName();
	});

	function processName (name) {
		IO.userData.name = DOM.$nameInput.val();
		IO.saveUserData();

		DOM.$nameInput.val('')
		setupAvatars();
		DOM.$namePanel.hide();
		DOM.$avatarPanel.show();
	}

	function setupAvatars () {
		var avatarsArray = [];

		for (var i = 79; i > 0; i--) {
			avatarsArray.push(i);
		}
		IO.shuffle(avatarsArray);

		var avatarsHtml = '';
		avatarsArray.forEach(function (el, index) {
			avatarsHtml += '<li class="avatar" style="background-image: url(images/avatars/' + el + '.gif)" data-avatar-id="' + el + '"></li>';
		});
		DOM.$avatarsList.html(avatarsHtml);

		DOM.$avatarsList.on('click', 'li', function() {
			var $this = $(this),
				avatarId = $this.data('avatar-id');

			$this.addClass('active');
			setTimeout(successfullyRegistered, 400);

			IO.userData.avatarId = avatarId;
			IO.saveUserData();
		});
	}

	function successfullyRegistered () {
		DOM.$avatarPanel.hide();
		DOM.$successPanel.show();

		setTimeout(function () {
			location.href = 'chat.html';
		}, 1000);
	}
}());