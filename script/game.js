/**
* Global namespace
*/
var IO = IO || {};


(function () {
	var gameIsInProgress = false,
		$beforeGameWrapper = $('#beforeGameWrapper'),
		$questionsWrapper = $('#questionsWrapper'),
		$resultsData = $('#resultsData'),
		$userAnswered = $('#userAnswered'),
		$points = $('#points'),
		$totalPoints = $('#totalPoints'),
		$afterGameWrapper = $('#afterGameWrapper'),
		$gameOver = $('#gameOver');

	// Sounds
	$('#enableSoundBtn').on('click', function () {
		$('#enableSoundWrapper').hide();

		setTimeout(function () {
			IO.sounds.success.play();
			IO.sounds.success.pause();
			IO.sounds.success.currentTime = 0;
		}, 100);
		
		setTimeout(function () {
			IO.sounds.wrong.play();
			IO.sounds.wrong.pause();
			IO.sounds.wrong.currentTime = 0;
		}, 1000);
	});

	IO.sounds = {
		success: new Audio('/sounds/success.mp3'),
		wrong: new Audio('/sounds/wrong.mp3')
	}

	// Join team
	$('.js-join-team-btn').on('click', function() {
		var team = $(this).data('team');

		IO.userData.team = team;
		localStorage.setItem('userData', JSON.stringify(IO.userData));

		IO.socket.emit('joinTeam', IO.userData);
	});

	$('.js-leave-team-btn').on('click', function() {
		var team = $(this).data('team');

		if (IO.userData.team == team) {
			IO.socket.emit('leaveTeam', IO.userData);
			IO.userData.team = null;
			localStorage.setItem('userData', JSON.stringify(IO.userData));
		}
	});

	var userTemplate = '<li class="list-group-item">\
						<div class="media">\
							<div class="media-left">\
								<div class="media-object avatar" style="background-image: url(images/avatars/{0}.gif)"></div>\
							</div>\
							<div class="media-body">\
								<h4 class="media-heading">{1}</h4>\
								<p class="media-subheading text-muted">{2}</p>\
							</div>\
						</div>\
					</li>';

	IO.socket.on('teams', function (teams) {
		var isInTheTeam = false;

		if (teams.length > 0) {
			teams.forEach(function (players, index) {
				var html = '';

				if (players) {
					players.forEach(function (player, index2) {
						if (player) {
							var u = player;

							html += userTemplate.format(u.avatarId, u.username, u.name);

							// Player is in the team
							if (u.guid == IO.userData.guid) {
								isInTheTeam = true;
							}

							$beforeGameWrapper.find('.js-team-list[data-team-list=' + index + ']').html(html);
						}
					});
				} else {
					$('.js-team-list').html('');
				}
			});

			if (isInTheTeam) {
				$('.js-join-team-btn').attr('disabled', true);
			} else {
				$('.js-join-team-btn').attr('disabled', false);
			}
		} else {
			$('.js-team-list').html('');
		}
	});

	// Start the game
	$('#startTheGameBtn').on('click', function() {
		var areYouSure = confirm('Do you really, really, really want to start the game?');

		if (areYouSure === true) {
			IO.socket.emit('startTheGame');
		}
	});

	IO.socket.on('gameStarted', function() {
		gameIsInProgress = true;
		$beforeGameWrapper.hide();
		$questionsWrapper.show();
	});

	// Before leaving the page
	$(window).on('beforeunload', function() {
		if (gameIsInProgress === true) {
			return 'Game is in progress.';
		}
	});

	// Questions
	var questionTemplate = '<p class="question-info clearfix"><span class="pull-left">Time: <span id="time">10</span>s</span><span class="pull-right">Question {0}</span></p>\
			<h4 class="question-text">{1}</h4>\
			<ul class="answers-list" id="answersList" data-question-number="{0}"></ul>',
		answerItemTemplate = '<li class="js-answer" data-answer-number="{0}">{0}. {1}</li>';

	IO.socket.on('question', function(data) {
		var questionHtml = questionTemplate.format(data.questionNumber, data.question),
			answersHtml = '',
			$questionHtml = $('<div/>').html(questionHtml);

		for (var answer in data.answers) {
			answersHtml += answerItemTemplate.format(answer, data.answers[answer]);
		}

		$questionHtml.find('#answersList').html(answersHtml);
		$questionsWrapper.html($questionHtml.html());
		$resultsData.hide();
	});

	// Answers
	$questionsWrapper.on('click', '.js-answer', function() {
		var $this = $(this),
			answerNumber = $this.data('answer-number'),
			questionNumber = $this.closest('#answersList').data('question-number');

		$this.addClass('active').siblings('.js-answer').removeClass('active');

		IO.socket.emit('answer', {
			user: IO.userData,
			questionNumber: questionNumber,
			answerNumber: answerNumber
		});
	});

	IO.socket.on('time', function(data) {
		$('#time').html(data);
	});

	// Show answer
	IO.socket.on('showAnswer', function(data) {
		var team = IO.userData.team,
			teamAnswer = data.teamAnswers.filter(function(el) {
				return el && el.team == team;
			})[0],
			u, userHtml;

		if (teamAnswer.user) {
			u = teamAnswer.user,
			userHtml = userTemplate.format(u.avatarId, u.username, u.name);			
		}

		// Select correct answer
		if (teamAnswer.isCorrect == true) {
			$questionsWrapper.find('[data-answer-number=' + teamAnswer.answer +']').addClass('correct');
			IO.sounds.success.play();
		} else {
			$questionsWrapper.find('[data-answer-number=' + teamAnswer.answer +']').addClass('wrong');
			$questionsWrapper.find('[data-answer-number=' + data.answer +']').addClass('correct');
			IO.sounds.wrong.play();
		}

		// Result data
		$points.html(teamAnswer.points);
		$totalPoints.html(teamAnswer.totalPoints);

		if (teamAnswer.user) {
			$userAnswered.html(userHtml);
		} else {
			$userAnswered.html('');			
		}

		$resultsData.show();
	});

	// Game over
	IO.socket.on('gameOver', function() {
		$questionsWrapper.hide();
		$resultsData.hide();
		$gameOver.show();
	});

	// Final results
	IO.socket.on('results', function(teams) {
		// Eliminate null
		teams.shift();

		if (teams.length > 0) {
			teams.forEach(function (team, index) {
				var html = '',
					tp = team.pop();

				if (team) {
					team.forEach(function (player, index2) {
						if (player) {
							var u = player;

							html += userTemplate.format(u.avatarId, u.username, u.name);

							$afterGameWrapper.find('.js-team-list[data-team-list=' + u.team + ']').html(html);
						}
					});
				}

				$afterGameWrapper.find('.js-team-total-points[data-team=' + tp.team + ']').html(tp.totalPoints);
			});
		}

		$gameOver.hide();
		$afterGameWrapper.show();
	});
}());