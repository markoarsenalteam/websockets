/**
* Global namespace
*/
var IO = IO || {};


(function () {
	var impressApi = impress(),
		$prevBtn = $('#prevBtn'),
		$nextBtn = $('#nextBtn');

	// Impress init
	impressApi.init();

	// Prev, next
	$prevBtn.on('click', function() {
		IO.socket.emit('prev');
	});

	$nextBtn.on('click', function() {
		IO.socket.emit('next');
	});

	IO.socket.on('prev', function() {
		console.log('prev')
		impressApi.prev();
	});

	IO.socket.on('next', function() {
		console.log('next')
		impressApi.next();
	});
}());