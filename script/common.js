/**
* Global namespace
*/
var IO = IO || {};


(function () {
	// Fast click
	FastClick.attach(document.body);

	// Connect
	IO.socket = io.connect('http://192.168.1.24:3700');

	IO.userData = localStorage.getItem('userData') ? JSON.parse(localStorage.getItem('userData')) : {};

	// Remove team
	delete IO.userData.team;
	localStorage.setItem('userData', JSON.stringify(IO.userData));

	IO.saveUserData = function () {
		IO.socket.emit('saveUserData', IO.userData);
	}

	IO.socket.on('userDataSaved', function(data) {
		localStorage.setItem('userData', JSON.stringify(data));
	});
}());