/**
* Global namespace
*/
var IO = IO || {};


(function () {
	// String format
	if (!String.prototype.format) {
		String.prototype.format = function () {
			var args = arguments;

			return this.replace(/{(\d+)}/g, function (match, number) { 
				return typeof args[number] != 'undefined' ? args[number] : match;
			});
		};
	}

	/**
	 * Shuffle array
	 * @param  { Array } 	array 	Array to shuffle
	 * @return { Array }       		Shuffled array
	 */
	IO.shuffle = function (array) {
		var currentIndex = array.length,
			temporaryValue,
			randomIndex;

		// While there remain elements to shuffle...
		while (0 !== currentIndex) {
			// Pick a remaining element...
			randomIndex = Math.floor(Math.random() * currentIndex);
			currentIndex -= 1;

			// And swap it with the current element.
			temporaryValue = array[currentIndex];
			array[currentIndex] = array[randomIndex];
			array[randomIndex] = temporaryValue;
		}

		return array;
	}
}());